﻿# **Need For Service** #

Need for Service je Android aplikacija koja ima za cilj da olakša proces servisiranja automobila kako za klijente tako i za vlasnike servis radnji. Aplikacija je zamisljena tako da olakša zakazivanje termina servisiranja, nalaženje najbližeg servisa i da omogući efikasniju komunikaciju izmedju korisnika i servisa.
* * *
## Tim: You'll Never Code Alone ##
* [Aleksa Djurovic](https://bitbucket.org/djuralfc/) 15100
* [Nemanja Djordjevic](https://bitbucket.org/necejfvr/) 15093
* [Bogdan Ilic](https://bitbucket.org/bolee95/) 15116
* [Nikola Gajic](https://bitbucket.org/wave995/) 15105